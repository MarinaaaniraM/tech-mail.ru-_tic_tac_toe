# Технопарк Mail@Ru
______________________________
* ##Курс: "Углубленное программирование на Java" 
* ##Курс: "Front-end разработка"

Совместный проект: **браузерная игра "Крестики-Нолики"**
####Студенты
- Титаевский Сергей, [mrGexogen] [2], Titarvskiy.S@gmail.com
- Тимофеева Марина, [MarinaaaniraM] [3], marinaaaniram@yandex.ru
- Кравцов Илья, [kic2512] [4], kravcov2512@gmail.com

![Alt text](https://bytebucket.org/MarinaaaniraM/tech-mail.ru-_tic_tac_toe/raw/201a01bd2f5dc4422c8936b2b0c1d3312bfe2fa0/static/img/user_o.png?token=823bc7551c4d8b1de6c41a1c8bbba18cc1799ad1)

* Ubuntu 12.04
* Java, jetty
* Js, jQuery, Backbone
* Unit tests
* Canvas
* Mobile

![Alt text](https://bytebucket.org/MarinaaaniraM/tech-mail.ru-_tic_tac_toe/raw/201a01bd2f5dc4422c8936b2b0c1d3312bfe2fa0/static/img/user_x.png?token=b4062acb587c76eabcd02e894d89e22c11c608ed)

[2]: https://github.com/mrGexogen
[3]: https://github.com/MarinaaaniraM
[4]:https://github.com/kic2512