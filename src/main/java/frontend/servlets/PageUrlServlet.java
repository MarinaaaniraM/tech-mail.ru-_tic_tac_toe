package frontend.servlets;

/**
 * Created by titaevskiy.s on 23.10.14
 */
public interface PageUrlServlet {
    String getPageUrl();
}
