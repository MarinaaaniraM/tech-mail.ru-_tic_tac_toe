package frontend;

/**
 * @author s.titaevskiy on 13.09.14.
 */
public enum ResponsesCode {
    OK,
    ALREADY_EXISTS,
    WRONG_LOGIN,
    BAD_INPUT
}
